# Revision history for servant-server-namedargs

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.

## 0.1.1.0 -- 2019-02-25

* Support for NamedBody'

## 0.1.1.1 -- 2019-03-22

* Deps bump for servant 0.16
